import React from 'react';
import Widgets from './components/Widgets';
import Plot from './components/Plot';
import DatePicker from './components/DatePicker'
import Grid from '@material-ui/core/Grid';

var sensors = [
  { text: 'Temperature', tag: 'temperature', unit: '\u00B0C', digits: '1' },
  { text: 'Humidity', tag: 'humidity', unit: '%', digits: '1' },
  { text: 'Light', tag: 'light', unit: 'lux', digits: '1' }
]
export default function App() {
  return (
    <Grid container spacing={10}>
      <Grid item xs={12}>
        <Widgets monitors={sensors} />
      </Grid>
      <Grid container spacing={10}>
        <Grid item xs={10}>
          <Plot />
        </Grid>
      </Grid>
      <Grid container justify='center'>
        <Grid item>
          <DatePicker />
        </Grid>
      </Grid>
      <Grid container justify='center'>
        <Grid item xs={5}>
          <div> {process.env.REACT_APP_NAME} v{process.env.REACT_APP_VERSION} </div>
        </Grid>
      </Grid>
    </Grid>
  )
}

