import React, { Component } from 'react'
import { ResponsiveContainer, AreaChart, XAxis, YAxis, Area, CartesianGrid, Tooltip } from 'recharts'
import CircularProgress from "@material-ui/core/CircularProgress";
import moment from 'moment';

const API = process.env.REACT_APP_REST_URL;

export default class Plot extends Component {
    constructor(props) {
        super()
        this.state = {
            data: null,
            loading: true,
            message: null
        }
        setTimeout(() => this.tick(), Math.floor(Math.random() * 100) + 100);
    }
    componentDidMount() {
        this.mounted = true;
        this.intervalID = setInterval(() => this.tick(), 30000);
    }
    componentWillUnmount() {
        this.mounted = false;
        clearInterval(this.intervalID);
    }
    tick() {
        let query = API + "history?sensor=temperature";
        fetch(query)
            .then((response) => response.json())
            .then((result) => {
                if (this.mounted) this.setState({ data: result, loading: false });
            })
            .catch((error) => {
                if (this.mounted) this.setState({ loading: true, message: error });
            });
    }
    render() {
        const { data } = this.state;
        // const entries = Object.values(data);
        if (this.state.message) {
            console.log(this.state.message);
            return <div> #ERR </div>;
        }
        if (this.state.loading) {
            return (
                <div>
                    <CircularProgress />
                </div>
            );
        }
        if (Object.entries(data).length === 0) {
            return null;
        }
        return (
            <div style={{ width: '100%', height: 200 }}>
                <ResponsiveContainer >
                    <AreaChart data={data} margin={{ top: 10, left: 10, right: 10 }}>
                        <Tooltip></Tooltip>
                        <CartesianGrid strokeDasharray='3 3'></CartesianGrid>
                        <XAxis dataKey='timestamp' minTickGap={30}
                            tickFormatter={(unitTime) => moment(unitTime).format('HH:mm')}>
                        </XAxis>
                        <YAxis></YAxis>
                        <Area dataKey="value" />
                    </AreaChart >
                </ResponsiveContainer >
            </div >
        )
    }

}
