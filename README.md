# MQTT SENSORS Dashboard

## CI/CD AUTOMATION (develop)

[![pipeline status](https://gitlab.com/dev-udo/mqtt-sensors/badges/develop/pipeline.svg)](https://gitlab.com/dev-udo/mqtt-sensors/-/commits/develop)
[![coverage report](https://gitlab.com/dev-udo/mqtt-sensors/badges/develop/coverage.svg)](https://gitlab.com/dev-udo/mqtt-sensors/-/commits/develop)

## Ports on Master branch

- 5000 - Freire
- 5001 - Ramos
- 5002 - Neto
- 5003 - Gonçalves
- 5004 - Lamy
- 5005 - Coutinho Sá

## Ports on Develop branch

- 5010 - Freire
- 5011 - Ramos
- 5012 - Neto
- 5013 - Gonçalves
- 5014 - Lamy
- 5015 - Coutinho Sá

## Running App

    docker run -dt -p 5000:5005 --rm --name "mqttudo dev-udo/mqtt-sensors:latest"
